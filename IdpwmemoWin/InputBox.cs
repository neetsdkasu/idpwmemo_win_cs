﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace IdpwmemoWin
{
    sealed class InputBox : Form
    {
        internal static string Show(string prompt, string title)
        {
            using (InputBox inputBox = new InputBox())
            {
                inputBox.label1.Text = prompt;
                inputBox.Text = title;
                if (inputBox.ShowDialog() == DialogResult.OK)
                {
                    string result = inputBox.textBox1.Text ?? "";
                    return result;
                }
                else
                {
                    return null;
                }
            }
        }

        private InputBox()
        {
            InitializeComponent();
        }

        private Label label1;
        private TextBox textBox1;
        private Button okButton;
        private Button cancelButton;

        private void okButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void InitializeComponent()
        {
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ClientSize = new Size(400, 100);
            this.StartPosition = FormStartPosition.CenterParent;

            this.label1 = new Label();
            this.textBox1 = new TextBox();
            this.okButton = new Button();
            this.cancelButton = new Button();

            this.label1.Dock = DockStyle.Fill;
            this.label1.AutoSize = true;
            this.label1.Padding = new Padding(5);

            this.textBox1.Dock = DockStyle.Fill;

            this.okButton.Dock = DockStyle.Right;
            this.okButton.Text = "OK";
            this.okButton.Click += new EventHandler(this.okButton_Click);

            this.cancelButton.Dock = DockStyle.Right;
            this.cancelButton.Text = "Cancel";

            var panel1 = new TableLayoutPanel();
            panel1.Dock = DockStyle.Fill;
            panel1.ColumnCount = 1;
            panel1.RowCount = 3;

            var panel2 = new Panel();
            panel2.Dock = DockStyle.Fill;
            panel2.Height = 30;

            panel2.Controls.AddRange(new Control[] {
                this.okButton,
                this.cancelButton
            });

            panel1.Controls.AddRange(new Control[] {
                this.label1,
                this.textBox1,
                panel2
            });

            panel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50));
            panel1.RowStyles.Add(new RowStyle(SizeType.Absolute, textBox1.Height));
            panel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 30));

            this.Controls.Add(panel1);

            this.AcceptButton = okButton;
            this.CancelButton = cancelButton;
        }
    }
}