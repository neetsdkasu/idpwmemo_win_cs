﻿namespace IdpwmemoWin
{
    class MemoInfo
    {
        internal string Name { get; set; }
        internal string Path { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}