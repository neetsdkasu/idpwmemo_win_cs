﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace IdpwmemoWin
{
    sealed class ImportBox : Form
    {
        new internal static Idpwmemo.IDPWMemo Show()
        {
            using (ImportBox dialog = new ImportBox())
            {
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    return dialog._memo;
                }
                else
                {
                    return null;
                }
            }
        }

        private Idpwmemo.IDPWMemo _memo = new Idpwmemo.IDPWMemo();

        private ImportBox()
        {
            InitializeComponent();
        }

        private TextBox textBox1;
        private Button pasteButton;
        private Button okButton;
        private Button cancelButton;

        private void pasteButton_Click(object sender, EventArgs e)
        {
            this.textBox1.Text = Clipboard.GetText();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            byte[] data = null;
            try
            {
                string text = this.textBox1.Text;
                if (String.IsNullOrWhiteSpace(text))
                {
                    MessageBox.Show("Data is empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                data = Convert.FromBase64String(text);
            }
            catch (Exception ex)
            {
                // Base64ではないテキストが指定された場合到達すると思われる
                MessageBox.Show("Failed: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string password = InputBox.Show("Import Password:", "Import - IDPWMemoWin");
            if (password == null)
            {
                return;
            }
            _memo.SetPassword(password);
            try
            {
                if (!_memo.LoadMemo(data))
                {
                    _memo.Clear();
                    MessageBox.Show("Failed: Wrong Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            catch (Exception ex)
            {
                // 到達シナリオ不明、予測できない
                MessageBox.Show("Failed: " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            this.DialogResult = DialogResult.OK;
        }

        private void InitializeComponent()
        {
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ClientSize = new Size(400, 400);
            this.StartPosition = FormStartPosition.CenterParent;

            this.textBox1 = new TextBox();
            this.pasteButton = new Button();
            this.okButton = new Button();
            this.cancelButton = new Button();

            this.textBox1.Dock = DockStyle.Fill;
            this.textBox1.Multiline = true;
            this.textBox1.ScrollBars = ScrollBars.Both;

            this.pasteButton.Dock = DockStyle.Right;
            this.pasteButton.Text = "Paste from clipboard";
            this.pasteButton.AutoSize = true;
            this.pasteButton.Click += new EventHandler(this.pasteButton_Click);

            this.okButton.Dock = DockStyle.Right;
            this.okButton.Text = "OK";
            this.okButton.Click += new EventHandler(this.okButton_Click);

            this.cancelButton.Dock = DockStyle.Right;
            this.cancelButton.Text = "Cancel";

            var panel1 = new TableLayoutPanel();
            panel1.Dock = DockStyle.Fill;
            panel1.ColumnCount = 1;
            panel1.RowCount = 2;

            var panel2 = new Panel();
            panel2.Dock = DockStyle.Fill;
            panel2.Height = 30;

            panel2.Controls.AddRange(new Control[] {
                this.pasteButton,
                this.okButton,
                this.cancelButton
            });

            panel1.Controls.AddRange(new Control[] {
                this.textBox1,
                panel2
            });

            panel1.RowStyles.Add(new RowStyle(SizeType.Percent, 70));
            panel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 30));

            this.Controls.Add(panel1);

            this.AcceptButton = okButton;
            this.CancelButton = cancelButton;
            this.Text = "Import - IDPWMemoWin";
        }
    }
}