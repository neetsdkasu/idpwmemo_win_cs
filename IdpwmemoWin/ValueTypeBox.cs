﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace IdpwmemoWin
{
    sealed class ValueTypeBox : Form
    {
        internal static Idpwmemo.ValueType Show(string data, Idpwmemo.ValueType valueType)
        {
            using (ValueTypeBox valueTypeBox = new ValueTypeBox())
            {
                valueTypeBox.label1.Text = "Value: " + data;
                valueTypeBox.comboBox1.SelectedIndex = (int)valueType;
                if (valueTypeBox.ShowDialog() == DialogResult.OK)
                {
                    byte index = unchecked((byte)valueTypeBox.comboBox1.SelectedIndex);
                    Idpwmemo.ValueType result = unchecked((Idpwmemo.ValueType)index);
                    return result;
                }
                else
                {
                    return valueType;
                }
            }
        }

        private ValueTypeBox()
        {
            InitializeComponent();
            List<string> list = new List<string>();
            for (byte i = 0; i < 8; i++)
            {
                list.Add(Idpwmemo.ValueTypeExtensions.TypeName((Idpwmemo.ValueType)i));
            }
            this.comboBox1.DataSource = list.AsReadOnly();
        }

        private Label label1;
        private ComboBox comboBox1;
        private Button okButton;
        private Button cancelButton;

        private void okButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void InitializeComponent()
        {
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ClientSize = new Size(400, 100);
            this.StartPosition = FormStartPosition.CenterParent;

            this.label1 = new Label();
            this.comboBox1 = new ComboBox();
            this.okButton = new Button();
            this.cancelButton = new Button();

            this.label1.Dock = DockStyle.Fill;
            this.label1.AutoSize = true;
            this.label1.Padding = new Padding(5);

            this.comboBox1.Dock = DockStyle.Top;
            this.comboBox1.Padding = new Padding(5);
            this.comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;

            this.okButton.Dock = DockStyle.Right;
            this.okButton.Text = "OK";
            this.okButton.Click += new EventHandler(this.okButton_Click);

            this.cancelButton.Dock = DockStyle.Right;
            this.cancelButton.Text = "Cancel";

            var panel1 = new TableLayoutPanel();
            panel1.Dock = DockStyle.Fill;
            panel1.ColumnCount = 1;
            panel1.RowCount = 3;

            var panel2 = new Panel();
            panel2.Dock = DockStyle.Fill;
            panel2.Height = 30;

            panel2.Controls.AddRange(new Control[] {
                this.okButton,
                this.cancelButton
            });

            panel1.Controls.AddRange(new Control[] {
                this.label1,
                this.comboBox1,
                panel2
            });

            panel1.RowStyles.Add(new RowStyle(SizeType.Percent, 30));
            panel1.RowStyles.Add(new RowStyle(SizeType.Absolute, comboBox1.Height + 5));
            panel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 30));

            this.Controls.Add(panel1);

            this.AcceptButton = okButton;
            this.CancelButton = cancelButton;
            this.Text = "Value Type Setting - IDPWMemoWin";
        }
    }
}