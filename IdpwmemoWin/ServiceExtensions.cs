﻿using System;

namespace IdpwmemoWin
{
    static class ServiceExtensions
    {
#if NET452
        private static readonly DateTimeOffset s_unixEpoch =
            new DateTimeOffset(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));

        private static DateTimeOffset GetDateTimeOffset(long time)
        {
            return s_unixEpoch.AddTicks(time * TimeSpan.TicksPerMillisecond);
        }
#else
        private static DateTimeOffset GetDateTimeOffset(long time)
        {
            return DateTimeOffset.FromUnixTimeMilliseconds(time);
        }
#endif

        internal static string DateTimeString(this Idpwmemo.Service service)
        {
            if (service.Time == 0L)
            {
                return "";
            }
            return GetDateTimeOffset(service.Time).LocalDateTime.ToString();
        }
    }
}