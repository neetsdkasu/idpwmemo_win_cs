﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace IdpwmemoWin
{
    sealed class ExportViewer : Form
    {
        internal static void Show(string base64)
        {
            using (ExportViewer viewer = new ExportViewer())
            {
                viewer.textBox1.Text = base64;
                viewer.ShowDialog();
            }
        }

        private ExportViewer()
        {
            InitializeComponent();
        }

        private TextBox textBox1;
        private Button copyButton;
        private Button closeButton;

        private void copyButton_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(this.textBox1.Text);
            MessageBox.Show("Copied!", "IDPWMemoWin", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void InitializeComponent()
        {
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ClientSize = new Size(400, 400);
            this.StartPosition = FormStartPosition.CenterParent;

            this.textBox1 = new TextBox();
            this.copyButton = new Button();
            this.closeButton = new Button();

            this.textBox1.Dock = DockStyle.Fill;
            this.textBox1.Multiline = true;
            this.textBox1.ScrollBars = ScrollBars.Both;
            this.textBox1.ReadOnly = true;

            this.copyButton.Dock = DockStyle.Right;
            this.copyButton.Text = "Copy to clipboard";
            this.copyButton.AutoSize = true;
            this.copyButton.Click += new EventHandler(this.copyButton_Click);

            this.closeButton.Dock = DockStyle.Right;
            this.closeButton.Text = "Close";
            this.closeButton.Click += new EventHandler(this.closeButton_Click);

            var panel1 = new TableLayoutPanel();
            panel1.Dock = DockStyle.Fill;
            panel1.ColumnCount = 1;
            panel1.RowCount = 2;

            var panel2 = new Panel();
            panel2.Dock = DockStyle.Fill;
            panel2.Height = 30;

            panel2.Controls.AddRange(new Control[] {
                this.copyButton,
                this.closeButton
            });

            panel1.Controls.AddRange(new Control[] {
                this.textBox1,
                panel2
            });

            panel1.RowStyles.Add(new RowStyle(SizeType.Percent, 70));
            panel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 30));

            this.Controls.Add(panel1);

            this.Text = "Export - IDPWMemoWin";
        }
    }
}