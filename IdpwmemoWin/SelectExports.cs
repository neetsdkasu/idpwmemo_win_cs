﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace IdpwmemoWin
{
    sealed class SelectExports : Form
    {
        internal static List<int> Show(Idpwmemo.IDPWMemo memo)
        {
            var targets = new List<int>();
            using (SelectExports dialog = new SelectExports())
            {
                for (int i = 0; i < memo.ServiceCount; i++)
                {
                    var service = memo.GetService(i);
                    var item = dialog.listView1.Items.Add(service.GetServiceName());
                    item.SubItems.Add(service.DateTimeString());
                    item.Tag = i;
                }
                DialogResult result = dialog.ShowDialog();
                if (result != DialogResult.OK)
                {
                    return null;
                }
                foreach (ListViewItem item in dialog.listView1.SelectedItems)
                {
                    targets.Add((int)item.Tag);
                }
            }
            return targets;
        }

        private SelectExports()
        {
            InitializeComponent();
        }

        private Label label1;
        private ListView listView1;
        private Button okButton;
        private Button cancelButton;

        private void okButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void InitializeComponent()
        {
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ClientSize = new Size(350, 400);
            this.StartPosition = FormStartPosition.CenterParent;

            this.label1 = new Label();
            this.listView1 = new ListView();
            this.okButton = new Button();
            this.cancelButton = new Button();

            this.label1.Dock = DockStyle.Fill;
            this.label1.AutoSize = true;
            this.label1.Padding = new Padding(5);
            this.label1.Text = "Select Exports";

            this.listView1.Dock = DockStyle.Fill;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.MultiSelect = true;
            this.listView1.Columns.Add("Name", 200, System.Windows.Forms.HorizontalAlignment.Left);
            this.listView1.Columns.Add("Last Update", -2, System.Windows.Forms.HorizontalAlignment.Left);

            this.okButton.Dock = DockStyle.Right;
            this.okButton.Text = "OK";
            this.okButton.Click += new EventHandler(this.okButton_Click);

            this.cancelButton.Dock = DockStyle.Right;
            this.cancelButton.Text = "Cancel";

            var panel1 = new TableLayoutPanel();
            panel1.Dock = DockStyle.Fill;
            panel1.ColumnCount = 1;
            panel1.RowCount = 3;

            var panel2 = new Panel();
            panel2.Dock = DockStyle.Fill;
            panel2.Height = 30;

            panel2.Controls.AddRange(new Control[] {
                this.okButton,
                this.cancelButton
            });

            panel1.Controls.AddRange(new Control[] {
                this.label1,
                this.listView1,
                panel2
            });

            panel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 30));
            panel1.RowStyles.Add(new RowStyle(SizeType.Percent, listView1.Height));
            panel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 30));

            this.Controls.Add(panel1);

            this.AcceptButton = okButton;
            this.CancelButton = cancelButton;
            this.Text = "Export - IDPWMemoWin";
        }
    }
}