﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace IdpwmemoWin
{
    sealed class SelectImports : Form
    {
        internal SelectImports(Idpwmemo.IDPWMemo memo)
        {
            InitializeComponent();
            foreach (string name in memo.GetServiceNames())
            {
                this.comboBox1.Items.Add(name);
            }
            if (this.comboBox1.Items.Count > 0)
            {
                this.comboBox1.SelectedIndex = 0;
            }
        }

        private Label label1;
        private ListView listView1;
        private Button okButton;
        private Button cancelButton;
        private ComboBox comboBox1;
        private RadioButton skipRB;
        private RadioButton addNewRB;
        private RadioButton replaceRB;

        private void okButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        internal int GetSelect(string text, Idpwmemo.Service service)
        {
            if (service.Time != 0L)
            {
                text += " ( " + service.DateTimeString() + " )";
            }
            this.label1.Text = text;
            this.addNewRB.Checked = true;
            this.listView1.BeginUpdate();
            this.listView1.Items.Clear();
            foreach (Idpwmemo.Value v in service.Values)
            {
                this.listView1.Items.Add(v.Data).SubItems.Add(v.TypeName);
            }
            this.listView1.EndUpdate();
            DialogResult result = this.ShowDialog();
            if (result == DialogResult.Cancel)
            {
                return -3;
            }
            if (this.skipRB.Checked)
            {
                return -2;
            }
            if (this.addNewRB.Checked)
            {
                return -1;
            }
            return this.comboBox1.SelectedIndex;
        }

        private void InitializeComponent()
        {
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ClientSize = new Size(350, 300);
            this.StartPosition = FormStartPosition.CenterParent;

            this.label1 = new Label();
            this.listView1 = new ListView();
            this.okButton = new Button();
            this.cancelButton = new Button();
            this.comboBox1 = new ComboBox();
            this.skipRB = new RadioButton();
            this.addNewRB = new RadioButton();
            this.replaceRB = new RadioButton();

            this.label1.Dock = DockStyle.Fill;
            this.label1.AutoSize = true;
            this.label1.Padding = new Padding(5);

            this.listView1.Dock = DockStyle.Fill;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.MultiSelect = false;
            this.listView1.Columns.Add("Value", 200, System.Windows.Forms.HorizontalAlignment.Left);
            this.listView1.Columns.Add("Type", -2, System.Windows.Forms.HorizontalAlignment.Left);

            this.okButton.Dock = DockStyle.Right;
            this.okButton.Text = "OK";
            this.okButton.Click += new EventHandler(this.okButton_Click);

            this.cancelButton.Dock = DockStyle.Right;
            this.cancelButton.Text = "Cancel";

            this.comboBox1.Dock = DockStyle.Fill;
            this.comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;

            this.skipRB.Dock = DockStyle.Top;
            this.skipRB.Text = "Skip (do not import this)";

            this.addNewRB.Dock = DockStyle.Top;
            this.addNewRB.Text = "Add new";
            this.addNewRB.Checked = true;

            this.replaceRB.Dock = DockStyle.Top;
            this.replaceRB.Text = "Replace exisiting service";

            var panel1 = new TableLayoutPanel();
            panel1.Dock = DockStyle.Fill;
            panel1.ColumnCount = 1;
            panel1.RowCount = 4;

            var panel2 = new Panel();
            panel2.Dock = DockStyle.Fill;
            panel2.Height = 30;

            var panel3 = new Panel();
            panel3.Dock = DockStyle.Fill;
            panel3.Height = 120;

            var panel4 = new Panel();
            panel4.Dock = DockStyle.Top;
            panel4.Padding = new Padding(20, 0, 0, 0);

            panel4.Controls.Add(this.comboBox1);

            panel2.Controls.AddRange(new Control[] {
                this.okButton,
                this.cancelButton
            });

            panel3.Controls.AddRange(new Control[] {
                panel4,
                this.replaceRB,
                this.addNewRB,
                this.skipRB
            });

            panel1.Controls.AddRange(new Control[] {
                this.label1,
                this.listView1,
                panel3,
                panel2
            });

            panel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 30));
            panel1.RowStyles.Add(new RowStyle(SizeType.Percent, listView1.Height));
            panel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 120));
            panel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 30));

            this.Controls.Add(panel1);

            this.AcceptButton = okButton;
            this.CancelButton = cancelButton;
            this.Text = "Import - IDPWMemoWin";
        }
    }
}