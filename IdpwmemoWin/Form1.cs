﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
// using System.Data;
using System.Drawing;
// using System.Linq;
// using System.Text;
using System.Text.RegularExpressions;
// using System.Threading.Tasks;
using System.Windows.Forms;

namespace IdpwmemoWin
{
    public partial class Form1 : Form
    {
        private string _memoDir;
        private Regex _memoNameRegex = new Regex(@"^[_a-zA-Z0-9\-\(\)\[\]]{1,50}$");
        private Idpwmemo.IDPWMemo _memo = new Idpwmemo.IDPWMemo();
        private MemoInfo _currentMemoInfo = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, System.EventArgs e)
        {
            InitializeMemoList();
            InitializeValueType();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this.ConfirmContinue())
            {
                e.Cancel = true;
            }
        }

        private void InitializeValueType()
        {
            List<string> list = new List<string>();
            for (byte i = 0; i < 8; i++)
            {
                list.Add(Idpwmemo.ValueTypeExtensions.TypeName((Idpwmemo.ValueType)i));
            }
            this.detailTypeComboBox.DataSource = list.AsReadOnly();
            this.secretTypeComboBox.DataSource = list.AsReadOnly();
            this.detailTypeComboBox.SelectedIndex = (int)Idpwmemo.ValueType.Id;
            this.secretTypeComboBox.SelectedIndex = (int)Idpwmemo.ValueType.Password;
        }

        private void InitializeMemoList()
        {
            // Java製IDPWMemoが作ったディレクトリがあるならそっちを優先
            string dir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            dir = System.IO.Path.Combine(dir, ".idpwmemo");
            if (!System.IO.Directory.Exists(dir))
            {
                dir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                dir = System.IO.Path.Combine(dir, "IDPWMemo");
                if (!System.IO.Directory.Exists(dir))
                {
                    System.IO.Directory.CreateDirectory(dir);
                }
            }
            _memoDir = dir;
            string[] files = System.IO.Directory.GetFiles(_memoDir);
            this.memoComboBox.BeginUpdate();
            foreach (string f in files)
            {
                if (System.IO.Path.GetExtension(f) != ".memo")
                {
                    continue;
                }
                MemoInfo mi = new MemoInfo();
                mi.Name = System.IO.Path.GetFileNameWithoutExtension(f);
                mi.Path = f;
                this.memoComboBox.Items.Add(mi);
            }
            this.memoComboBox.EndUpdate();
        }

        private void openMemoButton_Click(object sender, System.EventArgs e)
        {
            if (!this.ConfirmContinue())
            {
                return;
            }

            string memoName = this.memoComboBox.Text?.Trim();
            if (String.IsNullOrEmpty(memoName))
            {
                return;
            }
            if (!_memoNameRegex.IsMatch(memoName))
            {
                MessageBox.Show("Wrong Memo Name: " + memoName + "\n" +
                    "allow characters: A-Z a-z 0-9 - _ ( ) [ ]",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string password = InputBox.Show("Password?", "Open " + memoName);
            if (password == null)
            {
                return;
            }

            this.UnselectMemo();
            _memo.SetPassword(password);

            string path = System.IO.Path.Combine(_memoDir, memoName + ".memo");
            if (!System.IO.File.Exists(path))
            {
                _memo.NewMemo();
                _currentMemoInfo = new MemoInfo();
                _currentMemoInfo.Name = memoName;
                _currentMemoInfo.Path = path;
            }
            else
            {
                string[] files = System.IO.Directory.GetFiles(_memoDir, memoName + ".memo");
                if (files.Length == 0)
                {
                    _memo.Clear();
                    MessageBox.Show("Failed: Unknown Error (not found memo in list)", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (files.Length == 1)
                {
                    // おそらくWindowsのファイルシステム
                    path = files[0];
                }
                else
                {
                    // もしかするとWindows以外のファイルシステム
                    // そもそもSystem.Windows.FormsはWindows上でしか使えないはず
                    foreach (string filePath in files)
                    {
                        var fileName = System.IO.Path.GetFileNameWithoutExtension(filePath);
                        if (fileName == memoName)
                        {
                            path = filePath;
                            break;
                        }
                    }
                }
                memoName = System.IO.Path.GetFileNameWithoutExtension(path);
                foreach (object item in this.memoComboBox.Items)
                {
                    MemoInfo mi = (MemoInfo)item;
                    if (memoName == mi.Name)
                    {
                        _currentMemoInfo = mi;
                        break;
                    }
                }
                if (_currentMemoInfo == null)
                {
                    _memo.Clear();
                    MessageBox.Show("Failed: not found " + memoName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                this.memoComboBox.SelectedIndex = this.memoComboBox.FindStringExact(memoName);
                try
                {
                    byte[] data = System.IO.File.ReadAllBytes(path);
                    if (!_memo.LoadMemo(data))
                    {
                        _memo.Clear();
                        _currentMemoInfo = null;
                        MessageBox.Show("Failed: Wrong Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    // おそらくファイルアクセスに関する謎エラーで到達
                    _memo.Clear();
                    _currentMemoInfo = null;
                    MessageBox.Show("Failed: " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            this.SelectMemo(memoName);
        }

        private void SelectMemo()
        {
            this.SelectMemo(_currentMemoInfo.Name);
        }

        private void SelectMemo(string memoName)
        {
            this.servicesListView.BeginUpdate();
            this.servicesListView.Items.Clear();
            for (int si = 0; si < _memo.ServiceCount; si++)
            {
                var sv = _memo.GetService(si);
                var item = this.servicesListView.Items.Add(sv.GetServiceName());
                item.SubItems.Add(sv.DateTimeString());
                item.Tag = si;
            }
            this.servicesListView.EndUpdate();

            this.changePasswordButton.Enabled = true;
            this.exportButton.Enabled = true;
            this.importButton.Enabled = true;
            this.addServiceButton.Enabled = true;
            this.editServiceButton.Enabled = true;

            this.Text = memoName + " - IDPWMemoWin";
        }

        private void UnselectMemo()
        {
            this.servicesListView.Items.Clear();
            this.detailsListView.Items.Clear();
            this.secretsListView.Items.Clear();

            this.servicesListView.Sorting = SortOrder.None;

            this.detailTypeComboBox.Enabled = false;
            this.secretTypeComboBox.Enabled = false;

            this.changePasswordButton.Enabled = false;
            this.exportButton.Enabled = false;
            this.importButton.Enabled = false;
            this.addServiceButton.Enabled = false;
            this.editServiceButton.Enabled = false;
            this.saveButton.Enabled = false;
            this.addDetailButton.Enabled = false;
            this.showSecretsButton.Enabled = false;
            this.addSecretButton.Enabled = false;

            this.Text = "IDPWMemoWin";

            _memo.Clear();
            _currentMemoInfo = null;
        }

        private void changePasswordButton_Click(object sender, EventArgs e)
        {
            string newPassword = InputBox.Show("New Password:", "Change Password - IDPWMemoWin");
            if (newPassword == null)
            {
                return;
            }
            try
            {
                _memo.ChangePassword(newPassword);
                this.SaveMemo();
                MessageBox.Show("Saved!", "IDPWMemoWin", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                // おそらくファイルアクセスに関する謎エラーで到達
                MessageBox.Show("Failed: " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void exportButton_Click(object sender, EventArgs e)
        {
            if (!this.ConfirmContinue())
            {
                return;
            }
            this.UnselectService();
            List<int> targets = SelectExports.Show(_memo);
            if (targets == null || targets.Count == 0)
            {
                return;
            }
            string password = InputBox.Show("Export Password:", "Export - IDPWMemoWin");
            if (password == null)
            {
                return;
            }
            var export = new Idpwmemo.IDPWMemo();
            export.SetPassword(password);
            export.NewMemo();
            foreach (int index in targets)
            {
                _memo.SelectService(index);
                export.AddService(_memo);
            }
            _memo.UnselectService();
            byte[] data = export.Save();
            export.Clear();
            string base64 = Convert.ToBase64String(data, Base64FormattingOptions.InsertLineBreaks);
            ExportViewer.Show(base64);
        }

        private void importButton_Click(object sender, EventArgs e)
        {
            if (!this.ConfirmContinue())
            {
                return;
            }
            this.UnselectService();
            Idpwmemo.IDPWMemo import = ImportBox.Show();
            if (import == null)
            {
                return;
            }
            if (import.ServiceCount == 0)
            {
                MessageBox.Show("Import data is empty", "IDPWMemoWin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            int[] targets = new int[import.ServiceCount];
            using (SelectImports dialog = new SelectImports(_memo))
            {
                for (int i = 0; i < import.ServiceCount; i++)
                {
                    string text = String.Format("Import {0}/{1}", i + 1, import.ServiceCount);
                    targets[i] = dialog.GetSelect(text, import.GetService(i));
                    if (targets[i] == -3)
                    {
                        // Cancel
                        return;
                    }
                }
            }
            for (int i = 0; i < import.ServiceCount; i++)
            {
                switch (targets[i])
                {
                    case -2: // skip;
                        break;
                    case -1: // Add new
                        import.SelectService(i);
                        _memo.AddService(import);
                        break;
                    default: // replace
                        import.SelectService(i);
                        _memo.SetService(targets[i], import);
                        break;
                }
            }
            this.SelectMemo();
            try
            {
                this.SaveMemo();
                MessageBox.Show("Imported!", "IDPWMemoWin", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                // おそらくファイルアクセスに関する謎エラーで到達
                MessageBox.Show("Failed: " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool ConfirmContinue()
        {
            if (_memo.IsServiceEdited())
            {
                DialogResult result = MessageBox.Show("Discard changes and continue", "Confrim - IDPWMemoWin", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.Cancel)
                {
                    return false;
                }
            }
            return true;
        }

        private void addServiceButton_Click(object sender, EventArgs e)
        {
            if (!this.ConfirmContinue())
            {
                return;
            }

            string name = InputBox.Show("Service Name ?", "Add New Service - IDPWMemoWin");
            if (name == null)
            {
                return;
            }

            this.UnselectService();

            int newIndex = _memo.ServiceCount;

            _memo.AddNewService(name);
            try
            {
                this.SaveMemo();
            }
            catch (Exception ex)
            {
                // おそらくファイルアクセスに関する謎エラーで到達
                MessageBox.Show("Failed: " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            var item = this.servicesListView.Items.Add(name);
            item.SubItems.Add("");
            item.Tag = newIndex;

            var indices = this.servicesListView.SelectedIndices;
            indices.Clear();
            indices.Add(newIndex);

            this.SelectService();
        }

        private void editServiceButton_Click(object sender, EventArgs e)
        {
            if (!this.ConfirmContinue())
            {
                return;
            }
            this.SelectService();
        }

        private void UnselectService()
        {
            _memo.UnselectService();

            this.detailsListView.Items.Clear();
            this.secretsListView.Items.Clear();

            this.detailTypeComboBox.Enabled = false;
            this.secretTypeComboBox.Enabled = false;

            this.saveButton.Enabled = false;
            this.addDetailButton.Enabled = false;
            this.showSecretsButton.Enabled = false;
            this.addSecretButton.Enabled = false;

            foreach (ListViewItem item in this.servicesListView.Items)
            {
                if (item.BackColor == Color.Yellow)
                {
                    item.BackColor = this.servicesListView.BackColor;
                    break;
                }
            }
        }

        private void SelectService()
        {
            var indices = this.servicesListView.SelectedIndices;
            if (indices.Count == 0)
            {
                MessageBox.Show("Must select a service", "Error - IDPWMemoWin", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.UnselectService();

            int itemIndex = indices[0];
            int serviceIndex = (int)this.servicesListView.Items[itemIndex].Tag;

            try
            {
                _memo.SelectService(serviceIndex);
            }
            catch (Exception ex)
            {
                // 到達シナリオ不明、予測できず
                _memo.UnselectService();
                MessageBox.Show("Failed: " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var values = _memo.GetValues();
            if (values == null)
            {
                MessageBox.Show("Failed: Unknown Error (no values)", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.servicesListView.Items[itemIndex].BackColor = Color.Yellow;

            this.detailTypeComboBox.Enabled = true;

            this.saveButton.Enabled = true;
            this.addDetailButton.Enabled = true;
            this.showSecretsButton.Enabled = true;

            this.detailsListView.BeginUpdate();
            foreach (Idpwmemo.Value v in values)
            {
                var item = this.detailsListView.Items.Add(v.Data);
                item.SubItems.Add(v.TypeName);
                item.Tag = v;
            }
            this.detailsListView.EndUpdate();

            indices.Clear();
            this.detailsListView.Focus();
        }

        private void DeleteMemo()
        {
            string memoName = _currentMemoInfo.Name;
            DialogResult res = MessageBox.Show("Delete memo '" + memoName + "' ?", "Confirm - IDPWMemoWin", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (res != DialogResult.OK)
            {
                return;
            }

            try
            {
                if (System.IO.File.Exists(_currentMemoInfo.Path))
                {
                    System.IO.File.Delete(_currentMemoInfo.Path);
                }

                MessageBox.Show("Deleted!", "IDPWMemoWin", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                // おそらくファイルアクセスに関する謎エラーで到達
                MessageBox.Show("Failed: " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            this.memoComboBox.Items.Remove(_currentMemoInfo);

            this.UnselectMemo();
        }

        private void DeleteService()
        {
            string beforeName = _memo.GetService().GetServiceName();
            DialogResult res = MessageBox.Show("Delete service '" + beforeName + "' ?", "Confirm - IDPWMemoWin", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (res != DialogResult.OK)
            {
                return;
            }

            // 削除実行前にindexの確保が必須
            int index = _memo.SelectedServiceIndex;

            // 削除実行
            _memo.RemoveSelectedService();

            // listViewからの削除対象
            ListViewItem removeItem = null;

            foreach (ListViewItem item in this.servicesListView.Items)
            {
                int tmpIndex = (int)item.Tag;
                if (index == tmpIndex)
                {
                    // 削除対象の設定
                    removeItem = item;
                }
                else if (index < tmpIndex)
                {
                    item.Tag = tmpIndex - 1;
                }
            }

            // listViewからの削除
            removeItem?.Remove();

            this.SaveMemo();
            MessageBox.Show("Saved!", "IDPWMemoWin", MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.UnselectService();
        }

        private void UpdateService()
        {
            _memo.UpdateSelectedService();

            int index = _memo.SelectedServiceIndex;
            foreach (ListViewItem item in this.servicesListView.Items)
            {
                if (index != (int)item.Tag)
                {
                    continue;
                }
                Idpwmemo.Service service = _memo.GetService();
                item.Text = service.GetServiceName();
                item.SubItems[1].Text = service.DateTimeString();
                break;
            }

            this.SaveMemo();
            MessageBox.Show("Saved!", "IDPWMemoWin", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void SaveMemo()
        {
            _memo.ConvertV1ToV2();
            byte[] data = _memo.Save();
            bool found = System.IO.File.Exists(_currentMemoInfo.Path);
            System.IO.File.WriteAllBytes(_currentMemoInfo.Path, data);
            if (!found)
            {
                // 新規Memo
                this.memoComboBox.Items.Add(_currentMemoInfo);
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (_memo.GetSelectedService().ValidState)
                {
                    this.UpdateService();
                }
                else if (_memo.ServiceCount == 1)
                {
                    this.DeleteMemo();
                }
                else
                {
                    this.DeleteService();
                }
            }
            catch (Exception ex)
            {
                // おそらくファイルアクセスに関する謎エラーで到達
                MessageBox.Show("Failed: " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void addDetailButton_Click(object sender, EventArgs e)
        {
            string data = InputBox.Show("Type: " + this.detailTypeComboBox.Text, "Add New Value - IDPWMemoWin");
            if (data == null)
            {
                return;
            }
            var valueType = unchecked((Idpwmemo.ValueType)(byte)this.detailTypeComboBox.SelectedIndex);
            var newValue = new Idpwmemo.Value(valueType, data);
            _memo.GetValues().Add(newValue);
            var item = this.detailsListView.Items.Add(newValue.Data);
            item.SubItems.Add(newValue.TypeName);
            item.Tag = newValue;
        }

        private void showSecretsButton_Click(object sender, EventArgs e)
        {
            var secrets = _memo.GetSecrets();
            if (secrets == null)
            {
                MessageBox.Show("Failed: Unknown Error (no secrets)", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.secretTypeComboBox.Enabled = true;

            this.showSecretsButton.Enabled = false;
            this.addSecretButton.Enabled = true;

            this.secretsListView.BeginUpdate();
            foreach (Idpwmemo.Value v in secrets)
            {
                var item = this.secretsListView.Items.Add(v.Data);
                item.SubItems.Add(v.TypeName);
                item.Tag = v;
            }
            this.secretsListView.EndUpdate();

            this.secretsListView.Focus();
        }

        private void addSecretButton_Click(object sender, EventArgs e)
        {
            string data = InputBox.Show("Type: " + this.secretTypeComboBox.Text, "Add New Secret Value - IDPWMemoWin");
            if (data == null)
            {
                return;
            }
            var valueType = unchecked((Idpwmemo.ValueType)(byte)this.secretTypeComboBox.SelectedIndex);
            var newValue = new Idpwmemo.Value(valueType, data);
            _memo.GetSecrets().Add(newValue);
            var item = this.secretsListView.Items.Add(newValue.Data);
            item.SubItems.Add(newValue.TypeName);
            item.Tag = newValue;
        }

        private void servicesListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (this.servicesListView.Sorting == SortOrder.None)
            {
                this.servicesListView.Sorting = SortOrder.Ascending;
            }
        }

        private void servicesListView_DoubleClick(object sender, EventArgs e)
        {
            if (!this.ConfirmContinue())
            {
                return;
            }
            this.SelectService();
        }

        private void detailsListView_AfterLabelEdit(object sender, LabelEditEventArgs e)
        {
            if (e.Label == null)
            {
                // .NET5.0では未編集時にnullが来ないバグがある...(nullではなく空文字列が来て区別ができない…)
                // .NET5.0.9で修正される予定らしい
                // https://github.com/dotnet/winforms/milestone/48?closed=1
                // https://github.com/dotnet/winforms/pull/5192
                // https://github.com/dotnet/winforms/issues/5180
                return;
            }
            var v = (Idpwmemo.Value)this.detailsListView.Items[e.Item].Tag;
            v.Data = e.Label;
        }

        private void secretsListView_AfterLabelEdit(object sender, LabelEditEventArgs e)
        {
            if (e.Label == null)
            {
                // .NET5.0では未編集時にnullが来ないバグがある...(nullではなく空文字列が来て区別ができない…)
                // .NET5.0.9で修正される予定らしい
                // https://github.com/dotnet/winforms/milestone/48?closed=1
                // https://github.com/dotnet/winforms/pull/5192
                // https://github.com/dotnet/winforms/issues/5180
                return;
            }
            var v = (Idpwmemo.Value)this.secretsListView.Items[e.Item].Tag;
            v.Data = e.Label;
        }

        private void contextMenuStrip_Opening(Object sender, CancelEventArgs e)
        {
            var listView = this.contextMenuStrip.SourceControl as ListView;
            if (listView == null)
            {
                // Maybe Unreachable
                return;
            }
            if (listView.SelectedIndices.Count == 0)
            {
                this.contextMenuStrip.Items[0].Enabled = false;
                this.contextMenuStrip.Items[2].Enabled = false;
            }
            else
            {
                this.contextMenuStrip.Items[0].Enabled = true;
                this.contextMenuStrip.Items[2].Enabled = true;
            }
        }

        private void contextMenuStrip_ItemClicked(Object sender, ToolStripItemClickedEventArgs e)
        {
            var items = (this.contextMenuStrip.SourceControl as ListView)?.SelectedItems;
            if (items == null)
            {
                // Maybe Unreachable
                return;
            }
            var item = items[0];
            switch (this.contextMenuStrip.Items.IndexOf(e.ClickedItem))
            {
                case 0: // Copy to clipboard
                    Clipboard.SetText(item.Text);
                    MessageBox.Show("Copied!", "IDPWMemoWin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case 2: // Edit value
                    item.BeginEdit();
                    break;
                case 4: // Change Type
                    var v = (Idpwmemo.Value)item.Tag;
                    v.Type = ValueTypeBox.Show(v.Data, v.Type);
                    item.SubItems[1].Text = v.TypeName;
                    break;
                default:
                    // Cancel
                    break;
            }
        }
    }
}