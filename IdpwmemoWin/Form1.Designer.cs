﻿namespace IdpwmemoWin
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox servicesGroup;
        private System.Windows.Forms.GroupBox detailsGroup;
        private System.Windows.Forms.GroupBox secretsGroup;
        private System.Windows.Forms.Panel memoListPanel;
        private System.Windows.Forms.TableLayoutPanel servecesButtonPanel;
        private System.Windows.Forms.TableLayoutPanel detailsButtonPanel;
        private System.Windows.Forms.TableLayoutPanel secretssButtonPanel;
        private System.Windows.Forms.Label memoLabel;
        private System.Windows.Forms.ComboBox memoComboBox;
        private System.Windows.Forms.ComboBox detailTypeComboBox;
        private System.Windows.Forms.ComboBox secretTypeComboBox;
        private System.Windows.Forms.Button openMemoButton;
        private System.Windows.Forms.Button changePasswordButton;
        private System.Windows.Forms.Button exportButton;
        private System.Windows.Forms.Button importButton;
        private System.Windows.Forms.Button addServiceButton;
        private System.Windows.Forms.Button editServiceButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button addDetailButton;
        private System.Windows.Forms.Button showSecretsButton;
        private System.Windows.Forms.Button addSecretButton;
        private System.Windows.Forms.ListView servicesListView;
        private System.Windows.Forms.ListView detailsListView;
        private System.Windows.Forms.ListView secretsListView;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;


        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 550);

            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.memoListPanel = new System.Windows.Forms.Panel();
            this.servicesGroup = new System.Windows.Forms.GroupBox();
            this.detailsGroup = new System.Windows.Forms.GroupBox();
            this.secretsGroup = new System.Windows.Forms.GroupBox();
            this.servecesButtonPanel = new System.Windows.Forms.TableLayoutPanel();
            this.detailsButtonPanel = new System.Windows.Forms.TableLayoutPanel();
            this.secretssButtonPanel = new System.Windows.Forms.TableLayoutPanel();
            this.memoLabel = new System.Windows.Forms.Label();
            this.memoComboBox = new System.Windows.Forms.ComboBox();
            this.detailTypeComboBox = new System.Windows.Forms.ComboBox();
            this.secretTypeComboBox = new System.Windows.Forms.ComboBox();
            this.openMemoButton = new System.Windows.Forms.Button();
            this.changePasswordButton = new System.Windows.Forms.Button();
            this.exportButton = new System.Windows.Forms.Button();
            this.importButton = new System.Windows.Forms.Button();
            this.addServiceButton = new System.Windows.Forms.Button();
            this.editServiceButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.addDetailButton = new System.Windows.Forms.Button();
            this.showSecretsButton = new System.Windows.Forms.Button();
            this.addSecretButton = new System.Windows.Forms.Button();
            this.servicesListView = new System.Windows.Forms.ListView();
            this.detailsListView = new System.Windows.Forms.ListView();
            this.secretsListView = new System.Windows.Forms.ListView();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip();

            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;

            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;

            this.memoListPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.memoListPanel.Height = 30;

            this.servicesGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.servicesGroup.Text = "services";

            this.detailsGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailsGroup.Text = "details";

            this.secretsGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.secretsGroup.Text = "secrets";

            this.servecesButtonPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.servecesButtonPanel.Height = 30;
            this.servecesButtonPanel.ColumnCount = 5;
            this.servecesButtonPanel.RowCount = 1;

            this.detailsButtonPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.detailsButtonPanel.Height = 30;
            this.detailsButtonPanel.ColumnCount = 3;
            this.detailsButtonPanel.RowCount = 1;

            this.secretssButtonPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.secretssButtonPanel.Height = 30;
            this.secretssButtonPanel.ColumnCount = 3;
            this.secretssButtonPanel.RowCount = 1;

            this.memoLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.memoLabel.AutoSize = true;
            this.memoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.memoLabel.Text = "memo";

            this.memoComboBox.Dock = System.Windows.Forms.DockStyle.Fill;

            this.detailTypeComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.detailTypeComboBox.Enabled = false;

            this.secretTypeComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.secretTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.secretTypeComboBox.Enabled = false;

            this.openMemoButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.openMemoButton.Size = new System.Drawing.Size(50, 28);
            this.openMemoButton.Text = "OPEN";
            this.openMemoButton.Click += new System.EventHandler(this.openMemoButton_Click);

            this.changePasswordButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.changePasswordButton.Width = 60;
            this.changePasswordButton.Text = "CHPW";
            this.changePasswordButton.Enabled = false;
            this.changePasswordButton.Click += new System.EventHandler(this.changePasswordButton_Click);

            this.exportButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.exportButton.Width = 60;
            this.exportButton.Text = "EXP";
            this.exportButton.Enabled = false;
            this.exportButton.Click += new System.EventHandler(this.exportButton_Click);

            this.importButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.importButton.Width = 60;
            this.importButton.Text = "IMP";
            this.importButton.Enabled = false;
            this.importButton.Click += new System.EventHandler(this.importButton_Click);

            this.addServiceButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addServiceButton.Width = 60;
            this.addServiceButton.Text = "ADD";
            this.addServiceButton.Enabled = false;
            this.addServiceButton.Click += new System.EventHandler(this.addServiceButton_Click);

            this.editServiceButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editServiceButton.Width = 60;
            this.editServiceButton.Text = "EDIT";
            this.editServiceButton.Enabled = false;
            this.editServiceButton.Click += new System.EventHandler(this.editServiceButton_Click);

            this.saveButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saveButton.Width = 60;
            this.saveButton.Text = "SAVE";
            this.saveButton.Enabled = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);

            this.addDetailButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addDetailButton.Width = 60;
            this.addDetailButton.Text = "ADD";
            this.addDetailButton.Enabled = false;
            this.addDetailButton.Click += new System.EventHandler(this.addDetailButton_Click);

            this.showSecretsButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.showSecretsButton.Width = 60;
            this.showSecretsButton.Text = "SHOW";
            this.showSecretsButton.Enabled = false;
            this.showSecretsButton.Click += new System.EventHandler(this.showSecretsButton_Click);

            this.addSecretButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addSecretButton.Width = 60;
            this.addSecretButton.Text = "ADD";
            this.addSecretButton.Enabled = false;
            this.addSecretButton.Click += new System.EventHandler(this.addSecretButton_Click);

            this.servicesListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.servicesListView.View = System.Windows.Forms.View.Details;
            this.servicesListView.FullRowSelect = true;
            this.servicesListView.GridLines = true;
            this.servicesListView.MultiSelect = false;
            this.servicesListView.Columns.Add("Name", 200, System.Windows.Forms.HorizontalAlignment.Left);
            this.servicesListView.Columns.Add("Last Update", -2, System.Windows.Forms.HorizontalAlignment.Left);
            this.servicesListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.servicesListView_ColumnClick);
            this.servicesListView.DoubleClick += new System.EventHandler(this.servicesListView_DoubleClick);

            this.detailsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailsListView.View = System.Windows.Forms.View.Details;
            this.detailsListView.FullRowSelect = true;
            this.detailsListView.GridLines = true;
            this.detailsListView.MultiSelect = false;
            this.detailsListView.LabelEdit = true;
            this.detailsListView.ContextMenuStrip = this.contextMenuStrip;
            this.detailsListView.Columns.Add("Value", 200, System.Windows.Forms.HorizontalAlignment.Left);
            this.detailsListView.Columns.Add("Type", 120, System.Windows.Forms.HorizontalAlignment.Left);
            this.detailsListView.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.detailsListView_AfterLabelEdit);

            this.secretsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.secretsListView.View = System.Windows.Forms.View.Details;
            this.secretsListView.FullRowSelect = true;
            this.secretsListView.GridLines = true;
            this.secretsListView.MultiSelect = false;
            this.secretsListView.LabelEdit = true;
            this.secretsListView.ContextMenuStrip = this.contextMenuStrip;
            this.secretsListView.Columns.Add("Value", 200, System.Windows.Forms.HorizontalAlignment.Left);
            this.secretsListView.Columns.Add("Type", 120, System.Windows.Forms.HorizontalAlignment.Left);
            this.secretsListView.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.secretsListView_AfterLabelEdit);

            this.contextMenuStrip.Items.Add("Copy to clipboard");
            this.contextMenuStrip.Items.Add("-");
            this.contextMenuStrip.Items.Add("Edit value");
            this.contextMenuStrip.Items.Add("-");
            this.contextMenuStrip.Items.Add("Change type");
            this.contextMenuStrip.Items.Add("-");
            this.contextMenuStrip.Items.Add("Cancel");
            this.contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip_Opening);
            this.contextMenuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuStrip_ItemClicked);

            this.servecesButtonPanel.Controls.AddRange(new System.Windows.Forms.Control[] {
                this.changePasswordButton,
                this.exportButton,
                this.importButton,
                this.addServiceButton,
                this.editServiceButton
            });

            this.detailsButtonPanel.Controls.AddRange(new System.Windows.Forms.Control[] {
                this.saveButton,
                this.addDetailButton,
                this.detailTypeComboBox
            });

            this.secretssButtonPanel.Controls.AddRange(new System.Windows.Forms.Control[] {
                this.showSecretsButton,
                this.addSecretButton,
                this.secretTypeComboBox
            });

            this.servicesGroup.Controls.AddRange(new System.Windows.Forms.Control[] {
                this.servicesListView,
                this.servecesButtonPanel
            });

            this.detailsGroup.Controls.AddRange(new System.Windows.Forms.Control[] {
                this.detailsListView,
                this.detailsButtonPanel
            });

            this.secretsGroup.Controls.AddRange(new System.Windows.Forms.Control[] {
                this.secretsListView,
                this.secretssButtonPanel
            });

            this.splitContainer1.Panel1.Controls.Add(this.servicesGroup);
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);

            this.splitContainer2.Panel1.Controls.Add(this.detailsGroup);
            this.splitContainer2.Panel2.Controls.Add(this.secretsGroup);

            this.memoListPanel.Controls.AddRange(new System.Windows.Forms.Control[] {
                this.memoComboBox,
                this.openMemoButton,
                this.memoLabel
            });

            this.Controls.AddRange(new System.Windows.Forms.Control[] {
                this.splitContainer1,
                this.memoListPanel
            });

            this.splitContainer1.SplitterDistance = 200;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IDPWMemoWin";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
        }

        #endregion
    }
}

